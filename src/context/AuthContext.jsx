import { createContext, useState } from "react";

/**
 * Context for managing user authentication state.
 * 
 * @type {object}
 * @property {object} user - The authenticated user.
 * @property {Function} setUser - Function to set the authenticated user.
 */
export const AuthContext = createContext();

/**
 * Provider component for the AuthContext.
 * 
 * @component
 * @param {object} children - The child components to be wrapped by the provider.
 * @returns {JSX.Element} - The JSX element representing the AuthContextProvider.
 */
export const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const value = {
    user,
    setUser,
  };
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
