import { Navigate, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

/**
 * Component to check if the user is authenticated before rendering children components.
 * 
 * @component
 * @param {object} children - The children components to render if the user is authenticated.
 * @returns {JSX.Element} - The JSX elements representing the RequiresAuth component.
 */
function RequiresAuth({ children }) {
  let location = useLocation();
  const User = useSelector((state) => state.CurrentUserReducer);

  return User ? children : <Navigate to="/Auth" state={{ from: location }} />;
}

export default RequiresAuth;
