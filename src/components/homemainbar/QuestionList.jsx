import Questions from "./Questions";
import "./HomeMainbar.css";

/**
 * Component for rendering a list of questions.
 * 
 * @component
 * @param {Object[]} questionsList - The list of questions to render.
 * @param {Object} questionsList[].question - The question object to render.
 * @param {string} questionsList[].question._id - The unique identifier of the question.
 * @returns {JSX.Element} - The JSX elements representing the list of questions.
 */
function QuestionList({ questionsList }) {
  return (
    <div>
      {questionsList?.map((question) => (
        <Questions question={question} key={question._id} />
      ))}
    </div>
  );
}

export default QuestionList;
