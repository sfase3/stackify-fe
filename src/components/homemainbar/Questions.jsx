import { Link } from "react-router-dom";
import "./HomeMainbar.css";
import moment from "moment";

/**
 * Component for rendering details of a single question.
 * 
 * @component
 * @param {Object} question - The question object containing question details.
 * @param {string} question._id - The unique identifier of the question.
 * @param {string} question.questionTitle - The title of the question.
 * @param {string[]} question.questionTags - The tags associated with the question.
 * @param {number} question.noOfAnswers - The number of answers to the question.
 * @param {Date} question.askedOn - The date and time when the question was asked.
 * @param {string} question.userPosted - The user who posted the question.
 * @param {string[]} question.upVote - The IDs of users who upvoted the question.
 * @param {string[]} question.downVote - The IDs of users who downvoted the question.
 * @returns {JSX.Element} - The JSX elements representing the details of a single question.
 */
function Questions({ question }) {
  return (
    <div className="display-question-container">
      <div className="display-votes-ans">
        <p> {question?.upVote?.length - question?.downVote.length} </p>
        <p> votes </p>
      </div>
      <div className="display-votes-ans">
        <p> {question?.noOfAnswers} </p>
        <p> answers </p>
      </div>
      <div className="display-question-details">
        <Link
          to={`/Questions/${question?._id}`}
          className="question-title-link"
        >
          {" "}
          {question?.questionTitle}{" "}
        </Link>
        <div className="display-tags-time">
          <div className="display-tags">
            {question?.questionTags?.map((tag) => (
              <p key={tag}> {tag} </p>
            ))}
          </div>
          <p className="display-time">
            asked {moment(question?.askedOn).fromNow()} <span style={{color: '#ef8236'}}>{question?.userPosted}</span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Questions;
