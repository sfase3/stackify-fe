import "./HomeMainbar.css";
import { Link, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import QuestionList from "./QuestionList";
import { useContext, useMemo } from "react";
import { SearchContext } from "../../context/SearchContext";

/**
 * Component for rendering the main bar of the home page.
 * 
 * @component
 * @example
 * return (
 *   <HomeMainbar />
 * )
 */
function HomeMainbar() {
  const questionsList = useSelector((state) => state.QuestionReducer);
  const {searchValue} = useContext(SearchContext);
  const location = useLocation();

  /**
   * Filters the list of questions based on the search value.
   * 
   * @type {Array} filterData - The filtered list of questions.
   */
  // const filterData = useMemo(() => {
  //   return questionsList.data?.filter(el => el.questionTitle.includes(searchValue))
  // }, [searchValue]);

  return (
    <div className="main-bar">
      <div className="main-bar-header">
        {location.pathname === "/" ? (
          <h1>Top Questions</h1>
        ) : (
          <h1>All Questions</h1>
        )}
        <Link to="/AskQuestion" className="ask-btn">
          Ask a Question
        </Link>
      </div>
      <div>
        {questionsList?.data === null ? (
          <>
            <h1>Loading...</h1>
          </>
        ) : (
          <>
            <p>{questionsList?.data?.length} questions</p>
            <QuestionList questionsList={questionsList?.data} />
          </>
        )}
      </div>
    </div>
  );
}

export default HomeMainbar;
