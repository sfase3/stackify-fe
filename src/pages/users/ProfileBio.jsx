import "./UserProfile.css";

/**
 * ProfileBio component displays the user's profile bio including watched tags and about section.
 *
 * @component
 * @param {Object} props - The properties object.
 * @param {Object} props.currentProfile - The current profile object containing profile details.
 * @returns {JSX.Element} The rendered component.
 */
function ProfileBio({ currentProfile }) {
  return (
    <div>
      <div>
        {currentProfile?.tags?.length !== 0 ? (
          <>
            <h4>Tags watched</h4>
            {currentProfile?.tags?.map((tag) => {
              return <p key={tag}>{tag}</p>;
            })}
          </>
        ) : (
          <p> 0 tags watched </p>
        )}
      </div>
      <div>
        {currentProfile?.about ? (
          <>
            <h4>About</h4>
            <p> {currentProfile?.about} </p>
          </>
        ) : (
          <p>No bio found</p>
        )}
      </div>
    </div>
  );
}

export default ProfileBio;
