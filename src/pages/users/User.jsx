import { Link } from "react-router-dom";
import "./Users.css";

/**
 * User component renders a link to the user's profile.
 *
 * @component
 * @param {Object} props - The properties object.
 * @param {Object} props.user - The user object containing user details.
 * @param {string} props.user._id - The unique identifier of the user.
 * @param {string} props.user.name - The name of the user.
 * @returns {JSX.Element} The rendered component.
 */
function User({ user }) {
  return (
    <>
      <Link to={`/Users/${user?._id}`} className="user-profile-link">
        <h3> {user?.name?.charAt(0).toUpperCase()} </h3>
        <h5> {user?.name} </h5>
      </Link>
    </>
  );
}

export default User;
