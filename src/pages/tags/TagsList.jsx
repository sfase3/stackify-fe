import "./Tags.css";

/**
 * TagsList component displays a single tag with its name and description.
 *
 * @component
 * @param {Object} props - The properties object.
 * @param {Object} props.tag - The tag object containing details of the tag.
 * @param {string} props.tag.tagName - The name of the tag.
 * @param {string} props.tag.tagDesc - The description of the tag.
 * @returns {JSX.Element} The rendered component.
 */
function TagsList({ tag }) {
  return (
    <div className="tag">
      <h5>{tag.tagName}</h5>
      <p>{tag.tagDesc}</p>
    </div>
  );
}

export default TagsList;
