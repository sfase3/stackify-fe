import { Link, useParams } from "react-router-dom";
import moment from "moment";
import { useSelector, useDispatch } from "react-redux";

import Avatar from "../../components/avatar/Avatar";
import "./Questions.css";
import { deleteAnswer } from "../../actions/Question.action";

/**
 * DisplayAnswer component renders the answers for a specific question.
 * It allows users to share or delete answers if they are the owner.
 *
 * @component
 * @param {Object} props - The properties object.
 * @param {Object} props.question - The question object containing answers.
 * @param {function} props.handleShare - Function to handle sharing the answer.
 * @returns {JSX.Element} The rendered component.
 */
function DisplayAnswer({ question, handleShare }) {
  const User = useSelector((state) => state.CurrentUserReducer);
  const { id } = useParams();
  const dispatch = useDispatch();

  const handleDelete = (answerId, noOfAnswers) => {
    dispatch(deleteAnswer(id, answerId, noOfAnswers - 1));
  };

  return (
    <div>
      {question?.map((ans) => {
        return (
          <div className="display-ans" key={ans._id}>
            <p> {ans.answerBody} </p>
            <div className="question-actions-user">
              <div>
                <button type="button" onClick={handleShare}>
                  Share
                </button>
                {User?.result?._id === ans?.userId && (
                  <button
                    type="button"
                    onClick={() =>
                      handleDelete(ans?._id, question?.noOfAnswers)
                    }
                  >
                    Delete
                  </button>
                )}
              </div>
              <div>
                <p>answered {moment(ans.answeredOn).fromNow()} </p>
                <Link
                  to={`/Users/${ans?.userId}`}
                  className="user-link"
                  style={{ color: "#0086d8" }}
                >
                  <Avatar backgroundColor="green" px="8px" py="5px">
                    {" "}
                    {ans?.userAnswered?.charAt(0).toUpperCase()}{" "}
                  </Avatar>
                  <div>{ans.userAnswered}</div>
                </Link>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default DisplayAnswer;
